package com.michoExam.net.check_out_items;

import net.michoExam.tasks.FindItem;
import net.michoExam.tasks.LoginInThePage;
import net.michoExam.tasks.OpenTheBrowser;
import net.michoExam.utils.Login.LogginAccount;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.WithTag;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import static net.serenitybdd.screenplay.GivenWhenThen.and;
import static net.serenitybdd.screenplay.GivenWhenThen.givenThat;
import static net.serenitybdd.screenplay.GivenWhenThen.when;

@RunWith(SerenityRunner.class)
@WithTag("CheckoutStory")
public class CheckOutStory {
    private Actor micho = Actor.named("micho");

    @Managed
    private WebDriver hisbrowser;

    @Before
    public void michoCanBrowserInTheWeb(){
        micho.can(BrowseTheWeb.with(hisbrowser));
    }

    @Test
    @WithTag("CheckoutStory:test1")
    public void shouldBeAbleToCheckOut(){
        givenThat(micho).wasAbleTo(OpenTheBrowser.inTheApplication());
        when(micho).wasAbleTo(LoginInThePage.withCredentials(LogginAccount.email, LogginAccount.password));
        and(micho).wasAbleTo(FindItem.SortBy());
    }
}
