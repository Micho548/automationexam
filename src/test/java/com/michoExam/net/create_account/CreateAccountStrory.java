package com.michoExam.net.create_account;

import net.michoExam.questions.createAccount.TheAccountName;
import net.michoExam.tasks.NavigateTo;
import net.michoExam.tasks.OpenTheBrowser;
import net.michoExam.utils.create_account.User;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.WithTag;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.hamcrest.CoreMatchers.containsString;

@RunWith(SerenityRunner.class)
@WithTag("createAccountStory")
public class CreateAccountStrory {
    private Actor micho = Actor.named("micho");

    User data = new User();

    @Managed
    private WebDriver hisBrowser;

    @Before
    public void michoCanBrowserInTheWeb() {
        micho.can(BrowseTheWeb.with(hisBrowser));
    }

    @Test
    @WithTag("createAccountStory:test1")
    public void shouldBeAbleToCreateAccount(){

        givenThat(micho).wasAbleTo(OpenTheBrowser.inTheApplication());
        and(micho).wasAbleTo(NavigateTo.accountForm(data));
        then(micho).should(seeThat(TheAccountName.inTheHeader(), containsString(data.getFirstName())));
    }
}
