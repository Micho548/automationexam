package net.michoExam.tasks;

import net.michoExam.ui.authentication_page.AuthenticationPage;
import net.michoExam.ui.home_page.PrincipalPage;
import net.michoExam.utils.create_account.User;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class NavigateTo implements Task {

    private User user;

    public NavigateTo(User userE) {
        this.user = userE;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(PrincipalPage.SIGN_IN_LINK),
                Click.on(PrincipalPage.CREATE_ACCOUNT_LINK),
                Enter.theValue(user.getFirstName()).into(AuthenticationPage.FIRST_NAME_INPUT),
                Enter.theValue(user.getLastName()).into(AuthenticationPage.LAST_NAME_INPUT),
                Enter.theValue(user.geteMail()).into(AuthenticationPage.EMAIL_INPUT),
                Enter.theValue(user.getPassword()).into(AuthenticationPage.PASSWORD_INPUT),
                Enter.theValue(user.getBirthDate()).into(AuthenticationPage.BIRTHDAY_INPUT),
                Click.on(AuthenticationPage.SHOW_PASSWORD_BUTTON),
                Click.on(AuthenticationPage.SAVE_BUTTON)
        );
    }

    public  static NavigateTo accountForm(User userE){
        return instrumented(NavigateTo.class, userE);
    }
}
