package net.michoExam.tasks;

import net.michoExam.ui.home_page.PrincipalPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;

public class OpenTheBrowser implements Task {

    PrincipalPage principalPage;

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Open.browserOn(principalPage));
    }

    public static OpenTheBrowser inTheApplication() {
        return new OpenTheBrowser();
    }
}
