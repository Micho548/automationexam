package net.michoExam.tasks;

import net.michoExam.ui.home_page.PrincipalPage;
import net.michoExam.ui.login_page.LoginPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class LoginInThePage implements Task {
    private String email;
    private String password;

    public LoginInThePage(String email, String password) {
        this.email = email;
        this.password = password;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(PrincipalPage.SIGN_IN_LINK),
                Enter.theValue(email).into(LoginPage.EMAIL),
                Enter.theValue(password).into(LoginPage.PASSWORD),
                Click.on(LoginPage.SIGN_IN_BUTTON)
        );
    }

    public static LoginInThePage withCredentials (String email, String password){
        return instrumented(LoginInThePage.class, email, password);
    }
}
