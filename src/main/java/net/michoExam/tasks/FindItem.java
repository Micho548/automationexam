package net.michoExam.tasks;

import net.michoExam.ui.PageData;
import net.michoExam.ui.sort_accesories.SortAccesories;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Hover;
import net.serenitybdd.screenplay.waits.WaitUntil;
import net.serenitybdd.screenplay.waits.WaitUntilBuilder;
import org.openqa.selenium.support.ui.Wait;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class FindItem implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Hover.over(PageData.ACCESORIES_MENU),
                WaitUntil.the(PageData.HOME_ACCESORIES_MENU, isVisible()).forNoMoreThan(5).seconds(),
                Click.on(PageData.HOME_ACCESORIES_MENU),
                Click.on(SortAccesories.DROP_DOWN_SORT),
                Click.on(SortAccesories.LOW_HIGH_OPTION)
        );
    }

    public static FindItem SortBy(){
        return instrumented(FindItem.class);
    }
}
