package net.michoExam.ui.home_page;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.IFrame;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://demo.prestashop.com/en/?view=front")
public class PrincipalPage extends PageObject {

    private static IFrame frame = IFrame.withPath(By.id("framelive"));

    public static Target SIGN_IN_LINK = Target.the("Sign in link").inIFrame(frame)
            .locatedBy("#_desktop_user_info > div > a > span");

    public static Target CREATE_ACCOUNT_LINK = Target.the("Create account link").inIFrame(frame)
            .locatedBy("#content > div > a");

}
