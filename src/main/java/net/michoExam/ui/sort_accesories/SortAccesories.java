package net.michoExam.ui.sort_accesories;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.IFrame;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class SortAccesories extends PageObject {
    private static IFrame frame = IFrame.withPath(By.cssSelector("#framelive"));

    public static Target DROP_DOWN_SORT = Target.the("Drop Down Sort").inIFrame(frame)
            .located(By.xpath("//*[@id='js-product-list-top']/div[2]/div/div[1]/button"));

    public static Target LOW_HIGH_OPTION = Target.the("low high option").inIFrame(frame)
            .located(By.xpath("//*[@id='js-product-list-top']/div[2]/div/div[1]/div/a[4]"));
}
