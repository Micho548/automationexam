package net.michoExam.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.IFrame;
import net.serenitybdd.screenplay.targets.Target;

public class PageData extends PageObject {
    private static IFrame frame = IFrame.withPath(By.cssSelector("#framelive"));

    public static Target ACCOUNT_NAME = Target.the("login account name").inIFrame(frame)
            .located(By.cssSelector("#_desktop_user_info > div > a.account > span"));

    public static Target ACCESORIES_MENU = Target.the("Accesories menu").inIFrame(frame)
            .located(By.cssSelector("#category-6 > a"));

    public static Target HOME_ACCESORIES_MENU = Target.the("accesories menu").inIFrame(frame)
            .located(By.cssSelector("#category-8 > a"));


}
