package net.michoExam.ui.authentication_page;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.IFrame;
import net.serenitybdd.screenplay.targets.Target;

public class AuthenticationPage {


    public static IFrame frame = IFrame.withPath(By.id("framelive"));

    public static Target FIRST_NAME_INPUT = Target.the("First name input").inIFrame(frame)
            .located(By.name("firstname"));

    public static  Target LAST_NAME_INPUT = Target.the("Last name input").inIFrame(frame)
            .located(By.name("lastname"));

    public static Target EMAIL_INPUT = Target.the("Email input").inIFrame(frame)
            .located(By.name("email"));

    public static Target PASSWORD_INPUT = Target.the("password input").inIFrame(frame)
            .located(By.name("password"));

    public static Target BIRTHDAY_INPUT = Target.the("date of birthday input").inIFrame(frame)
            .located(By.name("birthday"));

    public static final Target SHOW_PASSWORD_BUTTON = Target.the("show password button").inIFrame(frame)
            .located(By.xpath("//*[@data-action='show-password']"));

    public static Target SAVE_BUTTON = Target.the("save button").inIFrame(frame)
            .locatedBy("#customer-form > footer > button");
}
