package net.michoExam.ui.login_page;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.IFrame;
import net.serenitybdd.screenplay.targets.Target;

public class LoginPage extends PageObject {
    private static IFrame frame = IFrame.withPath(By.cssSelector("#framelive"));

    public static Target EMAIL = Target.the("Email login").inIFrame(frame)
            .located(By.name("email"));

    public static Target PASSWORD = Target.the("Password login").inIFrame(frame)
            .located(By.name("password"));

    public static Target SIGN_IN_BUTTON = Target.the("Sign in Button").inIFrame(frame)
            .located(By.id("submit-login"));
}
