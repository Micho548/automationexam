package net.michoExam.utils.create_account;

import java.math.BigInteger;
import java.util.Random;

public class CreateAccountFunctions {
    public static String randNumString () {
        BigInteger num = new BigInteger(100,new Random());
        return String.valueOf(num);
    }

    public static String createEmail(){
        return "emailRandom"+randNumString()+"@gunit.com";
    }
}
