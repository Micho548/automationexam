package net.michoExam.utils.create_account;

import com.github.javafaker.Faker;

public class User {
    private String firstName;
    private String lastName;
    private String eMail;
    private String password;
    private String birthDate;

    public User() {
        Faker accountData = new Faker();


        this.firstName = accountData.name().firstName();
        this.lastName = accountData.name().lastName();
        this.eMail = CreateAccountFunctions.createEmail();
        this.password = accountData.name().fullName();
        this.birthDate = "02/15/1980";
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String geteMail() {
        return eMail;
    }

    public String getPassword() {
        return password;
    }

    public String getBirthDate() {
        return birthDate;
    }
}
