package net.michoExam.questions.createAccount;

import net.michoExam.ui.PageData;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class TheAccountName implements Question<String> {

    @Override
    public String answeredBy(Actor actor) {
        return Text.of(PageData.ACCOUNT_NAME).viewedBy(actor).asString();
    }

    public static Question<String> inTheHeader(){
        return new TheAccountName();
    }
}
